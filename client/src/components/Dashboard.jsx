import React, { Component } from "react";
import axios from "axios";
import { BrowserRouter, Route, Link, NavLink } from 'react-router-dom';
import ToDoItems from "./ToDoItems";
const api = axios.create({
  baseURL: "http://localhost:8080"
})

class Dashboard extends Component {

  constructor(props){
    super(props);

    this.state = {
      to_do_items: []
    }
  }
    componentDidMount() {
    }
    shouldComponentUpdate(nextProps, nextState) {
    }
    componentDidUpdate() {
    }
    componentWillUnmount() {
    }

    render() {
        const {
            onReset,
            onRestart,
            onAdd: onAddItem,
            onDelete: onDeleteItem
        } = this.props;

            return (
              <div className="DashboardApp">
              <ul>
                <li>
                  <Link to="/">Logout</Link>
                </li>
                <li>
                  <NavLink to="/dashboard" activeStyle={
                    { color:'green' }
                  }>Reload this page</NavLink>
                </li>
                <li>
                  <NavLink to="/to_do_items/1" activeStyle={
                    { color:'green' }
                  }>All TODO Items</NavLink>
                </li>
              </ul>
              <div><hr/></div>
              <h1>Welcome to the dashboard</h1>
              <div><hr/></div>
              </div>
            );
    }

  }

export default Dashboard;
