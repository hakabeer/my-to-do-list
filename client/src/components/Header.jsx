import React, { Component } from "react";
import { Link } from 'react-router-dom';
import { Table, Thead, Tbody, Tr, Th, Td } from 'react-super-responsive-table'
import 'react-super-responsive-table/dist/SuperResponsiveTableStyle.css'

class Header extends Component {
  state = {
    menu_items: [
      {id: -1, url: "/", name: "Website", selected: false },
      {id: 0, url: "/dashboard", name: "Dashboard", selected: false },
      {id: 1, url: "/to_do_items/1", name: "All Tasks", selected: false },
      {id: 2, url: "/to_do_items/2", name: "Critical Tasks", selected: false },
      {id: 3, url: "/to_do_items/3", name: "Upcoming Tasks", selected: false },
      {id: 4, url: "/to_do_items/4", name: "Pending Tasks", selected: false },
      {id: 5, url: "/to_do_items/5", name: "Recently Added Tasks", selected: false },
      {id: 6, url: "/to_do_items/6", name: "Recently Completed Tasks", selected: false },
      {id: 7, url: "/to_do_items/7", name: "Add New Task", selected: false }
    ]
  }
  constructor(props){
    super(props);
  }
    componentDidMount() {
    }
    shouldComponentUpdate(nextProps, nextState) {
          return true;
    }
    componentDidUpdate() {
    }
    componentWillUnmount() {
    }

    render() {

            return (
              <div>
                {/* <div className="new_feature">
                Feature available in next version
                </div> */}
                {/* <div className="col-md-12 top_bar">
                  <div className="navigation_bar">
                    <div className="menus col-md-8">
                      <ul>
                        <li> <a href="#"> <i className="fa fa-home"> </i> </a> </li>
                        <li> <a href="#"> Calendar </a> </li>
                        <li> <a href="#"> Organizations </a> </li>
                        <li> <a href="#"> Contacts </a> </li>
                        <li> <a href="#"> Products </a> </li>
                        <li> <a href="#"> Documents </a> </li>
                        <li> <a href="#"> Tickets </a> </li>
                        <li> <a href="#"> Projects </a> </li>
                        <li> <a href="#"> All&nbsp; <i className="fa fa-caret-down mini_icon"></i> </a> </li>
                      
                      </ul>
                    </div>
                    
                    <div className="col-md-4 logged_user_options">
                      <ul className="tr">
                        
                        
                        <li className="dropdown"> <a href="#" className="dropdown-toggle" id="dropdownMenu2"> Ahamed&nbsp; <i className="fa fa-caret-down mini_icon"></i> </a> 
                            <ul className="dropdown-menu" aria-labelledby="dropdownMenu2" data-toggle="dropdown" >
                  <li><a href="#"><i className="fa fa-user"></i>Contact</a></li>
                  <li><a href="#"><i className="fa fa-cogs"></i>Settings</a></li>
                  <li className="divider"></li>
                  <li><a href="#"><i className="fa fa-sign-out"></i>Logout</a></li>
                </ul>
                        </li>
                        <li> <a href="#"> <i className="fa fa-cog rounded"> </i> </a> </li>
                        <li> <a href="#"> <i className="fa fa-info rounded"> </i> </a> </li>
                        
                      </ul>
                    </div>
                  
                  </div>
                
                </div>		
                
                  <div className="header col-md-12">
                    <div className="inner_header col-md-12">
                      <div className="logo_container col-md-2">
                      <Link to="/"><img src="assets/images/logo.png" /></Link>
                      </div>
                      
                      <div className="col-md-10">
                        <div className="search_bar">
                        <div className="dropdown marg_right10">
                          <div className="all_records col-md-2 dropdown-toggle" id="dropdownMenu1" data-toggle="dropdown" aria-expanded="false">
                            All Records&nbsp; <i className="fa fa-caret-down fr"></i>
                          </div>
              <ul className="dropdown-menu" aria-labelledby="dropdownMenu1">
                  <li><a href="#"><i className="fa fa-user"></i>Contact</a></li>
                  <li><a href="#"><i className="fa fa-cogs"></i>Settings</a></li>
                  <li className="divider"></li>
                  <li><a href="#"><i className="fa fa-sign-out"></i>Logout</a></li>
                </ul>
                
                </div>
                
                <div className="dropdown marg_right10">
                          <div className="ddl_actions all_records col-md-1 dropdown-toggle" id="dropdownMenu3" data-toggle="dropdown" aria-expanded="false">
                            Actions&nbsp; <i className="fa fa-caret-down fr"></i>
                          </div>
              <ul className="dropdown-menu action_ddl" aria-labelledby="dropdownMenu3">
                  <li><a href="#"><i className="fa fa-user"></i>Constact</a></li>
                  <li><a href="#"><i className="fa fa-cogs"></i>Settings</a></li>
                  <li className="divider"></li>
                  <li><a href="#"><i className="fa fa-sign-out"></i>Logout</a></li>
                </ul>
                
                </div>
                
                  <div className="dropdown marg_right10">
                          <div className="ddl_actions all_records col-md-2 dropdown-toggle" id="dropdownMenu3" data-toggle="dropdown" aria-expanded="false">
                            All&nbsp; <i className="fa fa-filter fr"></i>
                          </div>
              <ul className="dropdown-menu filter_ddl" aria-labelledby="dropdownMenu3">
                  <li><a href="#"><i className="fa fa-user"></i>Constact</a></li>
                  <li><a href="#"><i className="fa fa-cogs"></i>Settings</a></li>
                  <li className="divider"></li>
                  <li><a href="#"><i className="fa fa-sign-out"></i>Logout</a></li>
                </ul>
                
                </div>
                
                    <div className="col-md-2 top_search_box">
                    <input type="text" placeholder="Search Keywords" />	  
                    <i className="fa fa-search"></i>
                    </div>
                    
                    <div className="col-md-2 adv_search">
                    Advanced Search
                    </div>
                          
                    <i className="fa fa-plus fr add_new"></i>
                            </div>
                      </div>
                    
                    </div>
                  
                  </div> */}

                  <div className="appTitleTable">
                        <div>Smart Recall App</div>
                  </div>
                  <div className="simpleLineBreak"></div>
                  <div className="menuTable">
                  {
                    this.state.menu_items.map(item => (
                      <div className="menuItem"><Link key={item.id} className="simpleLink" to={item.url}>{item.name}</Link></div>
                    ))
                  }
                  </div>
                  <div className="simpleLineBreak"></div>
              </div>              
            );
        }
    }

export default Header;
