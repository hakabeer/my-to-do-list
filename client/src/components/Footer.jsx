import React, { Component } from "react";

class Footer extends Component {

  constructor(props){
    super(props);
  }
    componentDidMount() {
    }
    shouldComponentUpdate(nextProps, nextState) {
          return true;
    }
    componentDidUpdate() {
    }
    componentWillUnmount() {
    }

    render() {
            return (
              <div>
                <div className="simpleLineBreak"></div>
                <div className="appFooter">
                  Copyright &copy; 2021. All rights reserved.
                </div>
              </div>
            );
        }
    }

export default Footer;