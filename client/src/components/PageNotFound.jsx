import React, { Component } from "react";
import { Link } from 'react-router-dom';

class PageNotFound extends Component {

  constructor(props){
    super(props);

    this.state = {
      to_do_items: []
    }
  }
    componentDidMount() {
    }
    shouldComponentUpdate(nextProps, nextState) {
    }
    componentDidUpdate() {
    }
    componentWillUnmount() {
    }

    render() {
        const {
            onReset,
            onRestart,
            onAdd: onAddItem,
            onDelete: onDeleteItem
        } = this.props;

            return (
                <div className="PageNotFound">Page Not Found. Go to <Link to="/">website</Link></div>
            );
    }

  }

export default PageNotFound;
