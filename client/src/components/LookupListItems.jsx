import React, { Component } from "react";
import axios from "axios";
import ToDoItem from "./ToDoItem";

const api = axios.create({
  baseURL: "http://localhost:8080"
})

class ToDoItems extends Component {

  constructor(props){
    super(props);

    this.state = {
      to_do_items: []
    }
  }
    componentDidMount() {
        console.log(`inside componentDidMount`);

        const that = this;

        const headersParams = {
            "Content-Type": "application/json",
            "x-access-token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2MDYyODU2MDMyODAsInVzZXIiOnsiaWQiOjEsImVtYWlsIjoiaGFrYWJlZXJAZ21haWwuY29tIiwiZmlyc3RuYW1lIjoiQWhhbWVkIiwibGFzdG5hbWUiOiJLYWJlZXIiLCJwaG9uZSI6Ijk4NDM1MjcyNDEiLCJsYXN0X2xvZ2luX3RpbWUiOm51bGwsInN0YXR1c19pZCI6Mn0sImNyZWF0ZWRBdCI6MTYwNTY4MDgwMzI4MH0.lnySP4-AuCF59Lr4Ot0EmcMhEmc7CU9cdDcGxRJa0QY"
        };

        api.get('/secured_api/to_do_items', {headers: headersParams}).then(result => {
            const to_do_items = (result && result.data && result.data.data) ? result.data.data : [];
            that.setState(prevState => ({
                to_do_items: to_do_items
            }));
            console.log(`to_do_items: ${JSON.stringify(that.state.to_do_items)}`);
        });
    }
    shouldComponentUpdate(nextProps, nextState) {
        console.log(`inside shouldComponentUpdate`);
        console.log(`nextProps: ${JSON.stringify(nextProps)}`);
        console.log(`nextState: ${JSON.stringify(nextState)}`);
        return true;
    }
    componentDidUpdate() {
      console.log(`inside componentDidUpdate`);
    }
    componentWillUnmount() {
        console.log(`inside componentWillUnmount`);
    }

    render() {
        const {
            onReset,
            onRestart,
            onAdd: onAddItem,
            onDelete: onDeleteItem
        } = this.props;

            return (
              <div>
                  <div>
                    <h1>{this.state.to_do_items.length}</h1>
                    <button
                      className="btn btn-success m-2"
                      onClick={onReset}
                      disabled={this.state.to_do_items.length === 0 ? "disabled" : ""}
                    >
                      <i className="fa fa-refresh" aria-hidden="true" />
                    </button>
                    <button
                      className="btn btn-primary m-2"
                      onClick={onRestart}
                      disabled={this.state.to_do_items.length !== 0 ? "disabled" : ""}
                    >
                      <i className="fa fa-recycle" aria-hidden="true" />
                    </button>
                  </div>
                  <div>
                      {
                      this.state.to_do_items.map(item => (
                          <ToDoItem
                              key={item.id}
                              item={item}
                              onAdd={onAddItem}
                              onDelete={onDeleteItem}
                          />
                      ))
                    }
                  </div>
              </div>
            );
        }
    }

export default ToDoItems;
