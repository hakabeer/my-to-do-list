import React, { Component } from "react";
import { Link } from 'react-router-dom';
import axios from "axios";
import ToDoItem from "./ToDoItem";
import Header from "./Header";
import Footer from "./Footer";

import { Table, Thead, Tbody, Tr, Th, Td } from 'react-super-responsive-table'
import 'react-super-responsive-table/dist/SuperResponsiveTableStyle.css'

const api = axios.create({
  baseURL: "http://localhost:8080"
})

class ToDoItems extends Component {

  constructor(props){
    super(props);

    this.state = {
      to_do_items: []
    }
  }
    componentDidMount() {
        console.log(`inside componentDidMount`);

        const that = this;

        const headersParams = {
            "Content-Type": "application/json",
            "x-access-token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2MTczODIyNzIyODksInVzZXIiOnsiaWQiOjEsImVtYWlsIjoiaGFrYWJlZXJAZ21haWwuY29tIiwiZmlyc3RuYW1lIjoiQWhhbWVkIiwibGFzdG5hbWUiOiJLYWJlZXIiLCJwaG9uZSI6Ijk4NDM1MjcyNDEiLCJsYXN0X2xvZ2luX3RpbWUiOm51bGwsInN0YXR1c19pZCI6Mn0sImNyZWF0ZWRBdCI6MTYxNDc5MDI3MjI4OX0.qs0-dS-N_hfZBVFQqpMmC5tk_bIj9--sYhB0lO8O810"
        };

        api.get('/secured_api/to_do_items', {headers: headersParams}).then(result => {
            const to_do_items = (result && result.data && result.data.data) ? result.data.data : [];
            that.setState(prevState => ({
                to_do_items: to_do_items
            }));
            console.log(`to_do_items: ${JSON.stringify(that.state.to_do_items)}`);
        });
    }
    shouldComponentUpdate(nextProps, nextState) {
        console.log(`inside shouldComponentUpdate`);
        console.log(`nextProps: ${JSON.stringify(nextProps)}`);
        console.log(`nextState: ${JSON.stringify(nextState)}`);
        return true;
    }
    componentDidUpdate() {
      console.log(`inside componentDidUpdate`);
    }
    componentWillUnmount() {
        console.log(`inside componentWillUnmount`);
    }

    render() {
        const {
            onReset,
            onRestart,
            onAdd: onAddItem,
            onDelete: onDeleteItem
        } = this.props;

            return (
              <div>

                  <Header />

                  {/* <div class="data_result_rows">
                    <div className="col-md-12 data_content_area">
                      
                      <div className="header_row col-md-12 boot_col">
                        <div className="col-md-1 check_select"> <label> <input type="checkbox" /> </label> </div>
                        <div className="col-md-1 boot_col"> Id </div>
                        <div className="col-md-4"> Task </div>
                        <div className="col-md-1"> Priority</div>
                        <div className="col-md-1"> Category </div>
                        <div className="col-md-2"> Assignee </div>
                        <div className="col-md-1"> Status </div>
                        <div className="col-md-1"> </div>
                        <div className="col-md-1"> </div>
                      </div>

                      <div className="col-md-12 empty_row">
                        {
                        this.state.to_do_items.map(item => (
                            <ToDoItem
                                key={item.id}
                                item={item}
                                onAdd={onAddItem}
                                onDelete={onDeleteItem}
                            />
                        ))
                      }
                      </div>
                    </div>
                  </div> */}
<Table>
	<Thead className="taskTableHead">
		<Tr>
			<Th>Id</Th>
			<Th>Task</Th>
			<Th>Priority</Th>
			<Th>Category</Th>
			<Th>Assignee</Th>
      <Th>Due Date</Th>
      <Th>Reminder Time</Th>
			<Th>Status</Th>
		</Tr>
	</Thead>
	<Tbody className="taskTableBody">
  {
    this.state.to_do_items.map(item => (    
      <Tr>
        <Td>{item.id}</Td>
        <Td>{item.name}</Td>
        <Td>{item.priority}</Td>
        <Td>{item.category}</Td>
        <Td>{item.assignee}</Td>
        <Td>{item.formatted_due_date}</Td>
        <Td>{item.formatted_reminder_at}</Td>
        <Td>{item.status}</Td>
      </Tr>
    ))
  }
	</Tbody>
</Table>
                
                  <Footer />
                </div>
            );
        }
    }

export default ToDoItems;
