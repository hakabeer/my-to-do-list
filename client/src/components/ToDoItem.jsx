import React, { Component } from "react";

class ToDoItem extends Component {
  render() {
    return (
      <div>
        <div className="row">


				<div class="single_result_row col-md-12 boot_col"> 
					<div class="col-md-1 check_select"> <label> <input type="checkbox" /> </label> </div>
					<div class="col-md-1 boot_col"> {this.props.item.id}</div>
					<div class="col-md-4"> {this.props.item.name} </div>
					<div class="col-md-1"> {this.props.item.priority} </div>
					<div class="col-md-1"> {this.props.item.category} </div>
					<div class="col-md-2"> {this.props.item.assignee} </div>
					<div class="col-md-1"> {this.props.item.status} </div>
					
					<div class="fr action_btn">
						<i class="fa fa-pencil fl edit" title="Edit"></i>
						<i class="fa fa-trash fl del" title="Delete"></i>
					</div>
				</div>
        </div>
      </div>
    );
  }

  getBadgeClasses = () => {
    let classes = "badge m-2 badge-";
    classes += this.props.item.value === 0 ? "warning" : "primary";
    return classes;
  };

  formatCount = () => {
    const { value } = this.props.item;
    return value === 0 ? "Zero" : value;
  };
}

export default ToDoItem;
