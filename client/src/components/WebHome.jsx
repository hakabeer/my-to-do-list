import React, { Component } from 'react';
import { Link, NavLink } from 'react-router-dom';

class WebHome extends Component {

  state = {
    logged_in: false,
    first_two_items: []
  }

  loginHandle = () => {
    this.setState(prevState => ({
      logged_in: !prevState.logged_in
    }))
  }

  render() {
    return (
        <div className="App">
          <ul>
            <li>
              <Link to="/" >Website Home page</Link>
            </li>
            <li>
              <NavLink to="/dashboard" activeStyle={
                { color:'green' }
              }>Login to Dashboard</NavLink>
            </li>
          </ul>
          <div><hr/></div>
          <h1>Welcome Home (this is the main page of the website)</h1>
        </div>      
    );
  }
}

export default WebHome;
