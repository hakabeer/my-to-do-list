import React, { Component } from "react";

class ToDoItem extends Component {
  render() {
    return (
      <div>
        <div className="row">
          <div className="col-md-6">
            <span style={{ fontSize: 24 }} className={this.getBadgeClasses()}>
              {this.props.item.id}
            </span>
            <span style={{ fontSize: 24 }} className={this.getBadgeClasses()}>
              {this.props.item.name}
            </span>
            <span style={{ fontSize: 24 }} className={this.getBadgeClasses()}>
              {this.props.item.priority}
            </span>
            <span style={{ fontSize: 24 }} className={this.getBadgeClasses()}>
              {this.props.item.category}
            </span>
            <span style={{ fontSize: 24 }} className={this.getBadgeClasses()}>
              {this.props.item.assignee}
            </span>
            <span style={{ fontSize: 24 }} className={this.getBadgeClasses()}>
              {this.props.item.status}
            </span>
          </div>
          <div className="col-md-6">
            <button
              className="btn btn-secondary"
              onClick={() => this.props.onIncrement(this.props.item.name)}
            >
              <i className="fa fa-plus-circle" aria-hidden="true" />
            </button>
            <button
              className="btn btn-danger"
              onClick={() => this.props.onDelete(this.props.item.id)}
            >
              <i className="fa fa-trash-o" aria-hidden="true" />
            </button>
          </div>
        </div>
      </div>
    );
  }

  getBadgeClasses = () => {
    let classes = "badge m-2 badge-";
    classes += this.props.item.value === 0 ? "warning" : "primary";
    return classes;
  };

  formatCount = () => {
    const { value } = this.props.item;
    return value === 0 ? "Zero" : value;
  };
}

export default ToDoItem;
