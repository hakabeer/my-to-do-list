import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import { BrowserRouter, Route, Link, NavLink, Switch, Redirect} from 'react-router-dom';
import Dashboard from "./components/Dashboard";
import ToDoItems from "./components/ToDoItems";
import PageNotFound from "./components/PageNotFound";

// import "bootstrap/dist/css/bootstrap.css";
// import "font-awesome/css/font-awesome.css";

const routs = (
    <BrowserRouter>
        <div className="App">
        <Switch>
            <Route path="/" exact strict component={App} />
            <Route path="/dashboard" exact strict component={Dashboard} />
            <Route exact path="/to_do_items/:type" component={ToDoItems} />
            <Route component={PageNotFound} />
        </Switch>
        </div>
    </BrowserRouter>
);

ReactDOM.render(routs, document.getElementById("root"));
