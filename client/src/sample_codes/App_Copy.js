import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link, NavLink, Prompt, Redirect} from 'react-router-dom';
import ToDoItems from "../components/ToDoItems";

class App extends Component {

  state = {
    logged_in: false,
    first_two_items: []
  }

  loginHandle = () => {
    this.setState(prevState => ({
      logged_in: !prevState.logged_in
    }))
  }

  render() {
    return (
      <Router>
        <div className="App">
        <ul>
          <li>
            <Link to="/" exact="true">Website Home page</Link>
          </li>
          <li>
            <Link to="/about" exact="true">About</Link>
          </li>
          <li>
            <Link to="/home/Senthil" exact="true">Application Home</Link>
          </li>
          <li>
            <NavLink to="/" exact activeStyle={
              { color:'green' }
            }>Website Hoe page (using NavLink)</NavLink>
          </li>
          <li>
            <NavLink to="/about" exact activeStyle={
              { color:'green' }
            }>About (using NavLink)</NavLink>
          </li>
          <li>
            <NavLink to="/user/john" exact activeStyle={
              { color:'green' }
            }>Application Home page (using NavLink)</NavLink>
          </li>
          <li>
            <NavLink to="/to_do_items" exact activeStyle={
              { color:'green' }
            }>TODO Items</NavLink>
          </li>
        </ul>
        <h1><input type="text" value={`${this.state.logged_in}`} readOnly></input></h1>
        <input type="button" value={this.state.logged_in ? 'Logout' : 'Login'} onClick={this.loginHandle.bind(this)}/>
        <Prompt
          when={!this.state.logged_in}
          message={(location)=>{
              return 'Please Login'
            }}
        />
        <Route path="/" exact render={
                  () => {
                    return ( <h1>Welcome Home (this is the main page of the website)</h1>);
                  }
                }/>

        <Route path="/about" exact strict render={
                  () => {
                    return ( <h1>About Page</h1>);
                  }
                }/>
        <Route path="/to_do_items" exact strict render={
                  () => {
                    return ( <ToDoItems />);
                  }
                }/>

      <Route path="/home/:name" exact strict render={
                ({match}) => {

                  if(match.params.name !== 'Senthil'){
                    return (<Redirect to="/about" />)
                  } else {
                    return ( <div>
                                  <h1>Application Home Page (after the login)</h1>
                                  <h1>Hi {match.params.name}</h1>
                              </div>
                          );
                    }
                  }
                }/>

        </div>
      </Router>
    );
  }
}

export default App;
