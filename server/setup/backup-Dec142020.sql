PGDMP         4                 x            smart_recall     12.5 (Ubuntu 12.5-1.pgdg16.04+1)    12.0 �    5           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                      false            6           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                      false            7           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                      false            8           1262    7036842    smart_recall    DATABASE     �   CREATE DATABASE smart_recall WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'en_US.UTF-8' LC_CTYPE = 'en_US.UTF-8';
    DROP DATABASE smart_recall;
                kabeer_smart_recall    false            9           0    0    DATABASE smart_recall    ACL     A   REVOKE CONNECT,TEMPORARY ON DATABASE smart_recall FROM PUBLIC;
                   kabeer_smart_recall    false    4152            	            2615    7036893    hdb_catalog    SCHEMA        CREATE SCHEMA hdb_catalog;
    DROP SCHEMA hdb_catalog;
                kabeer_smart_recall    false            
            2615    7036894 	   hdb_views    SCHEMA        CREATE SCHEMA hdb_views;
    DROP SCHEMA hdb_views;
                kabeer_smart_recall    false            :           0    0    SCHEMA public    ACL     �   REVOKE ALL ON SCHEMA public FROM postgres;
REVOKE ALL ON SCHEMA public FROM PUBLIC;
GRANT ALL ON SCHEMA public TO kabeer_smart_recall;
GRANT ALL ON SCHEMA public TO PUBLIC;
                   kabeer_smart_recall    false    4            ;           0    0    LANGUAGE plpgsql    ACL     1   GRANT ALL ON LANGUAGE plpgsql TO kabeer_smart_recall;
                   postgres    false    808                        3079    7036895    pgcrypto 	   EXTENSION     <   CREATE EXTENSION IF NOT EXISTS pgcrypto WITH SCHEMA public;
    DROP EXTENSION pgcrypto;
                   false            <           0    0    EXTENSION pgcrypto    COMMENT     <   COMMENT ON EXTENSION pgcrypto IS 'cryptographic functions';
                        false    2            #           1255    7037144    check_violation(text)    FUNCTION     �   CREATE FUNCTION hdb_catalog.check_violation(msg text) RETURNS boolean
    LANGUAGE plpgsql
    AS $$
  BEGIN
    RAISE check_violation USING message=msg;
  END;
$$;
 5   DROP FUNCTION hdb_catalog.check_violation(msg text);
       hdb_catalog          kabeer_smart_recall    false    9                       1255    7037092 "   hdb_schema_update_event_notifier()    FUNCTION     '  CREATE FUNCTION hdb_catalog.hdb_schema_update_event_notifier() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
  DECLARE
    instance_id uuid;
    occurred_at timestamptz;
    invalidations json;
    curr_rec record;
  BEGIN
    instance_id = NEW.instance_id;
    occurred_at = NEW.occurred_at;
    invalidations = NEW.invalidations;
    PERFORM pg_notify('hasura_schema_update', json_build_object(
      'instance_id', instance_id,
      'occurred_at', occurred_at,
      'invalidations', invalidations
      )::text);
    RETURN curr_rec;
  END;
$$;
 >   DROP FUNCTION hdb_catalog.hdb_schema_update_event_notifier();
       hdb_catalog          kabeer_smart_recall    false    9                       1255    7037008 -   inject_table_defaults(text, text, text, text)    FUNCTION       CREATE FUNCTION hdb_catalog.inject_table_defaults(view_schema text, view_name text, tab_schema text, tab_name text) RETURNS void
    LANGUAGE plpgsql
    AS $$
    DECLARE
        r RECORD;
    BEGIN
      FOR r IN SELECT column_name, column_default FROM information_schema.columns WHERE table_schema = tab_schema AND table_name = tab_name AND column_default IS NOT NULL LOOP
          EXECUTE format('ALTER VIEW %I.%I ALTER COLUMN %I SET DEFAULT %s;', view_schema, view_name, r.column_name, r.column_default);
      END LOOP;
    END;
$$;
 s   DROP FUNCTION hdb_catalog.inject_table_defaults(view_schema text, view_name text, tab_schema text, tab_name text);
       hdb_catalog          kabeer_smart_recall    false    9            "           1255    7037104 .   insert_event_log(text, text, text, text, json)    FUNCTION     �  CREATE FUNCTION hdb_catalog.insert_event_log(schema_name text, table_name text, trigger_name text, op text, row_data json) RETURNS text
    LANGUAGE plpgsql
    AS $$
  DECLARE
    id text;
    payload json;
    session_variables json;
    server_version_num int;
  BEGIN
    id := gen_random_uuid();
    server_version_num := current_setting('server_version_num');
    IF server_version_num >= 90600 THEN
      session_variables := current_setting('hasura.user', 't');
    ELSE
      BEGIN
        session_variables := current_setting('hasura.user');
      EXCEPTION WHEN OTHERS THEN
                  session_variables := NULL;
      END;
    END IF;
    payload := json_build_object(
      'op', op,
      'data', row_data,
      'session_variables', session_variables
    );
    INSERT INTO hdb_catalog.event_log
                (id, schema_name, table_name, trigger_name, payload)
    VALUES
    (id, schema_name, table_name, trigger_name, payload);
    RETURN id;
  END;
$$;
 z   DROP FUNCTION hdb_catalog.insert_event_log(schema_name text, table_name text, trigger_name text, op text, row_data json);
       hdb_catalog          kabeer_smart_recall    false    9            �            1259    7037040    event_invocation_logs    TABLE     �   CREATE TABLE hdb_catalog.event_invocation_logs (
    id text DEFAULT public.gen_random_uuid() NOT NULL,
    event_id text,
    status integer,
    request json,
    response json,
    created_at timestamp without time zone DEFAULT now()
);
 .   DROP TABLE hdb_catalog.event_invocation_logs;
       hdb_catalog         heap    kabeer_smart_recall    false    2    9            �            1259    7037022 	   event_log    TABLE       CREATE TABLE hdb_catalog.event_log (
    id text DEFAULT public.gen_random_uuid() NOT NULL,
    schema_name text NOT NULL,
    table_name text NOT NULL,
    trigger_name text NOT NULL,
    payload jsonb NOT NULL,
    delivered boolean DEFAULT false NOT NULL,
    error boolean DEFAULT false NOT NULL,
    tries integer DEFAULT 0 NOT NULL,
    created_at timestamp without time zone DEFAULT now(),
    locked boolean DEFAULT false NOT NULL,
    next_retry_at timestamp without time zone,
    archived boolean DEFAULT false NOT NULL
);
 "   DROP TABLE hdb_catalog.event_log;
       hdb_catalog         heap    kabeer_smart_recall    false    2    9            �            1259    7037009    event_triggers    TABLE     �   CREATE TABLE hdb_catalog.event_triggers (
    name text NOT NULL,
    type text NOT NULL,
    schema_name text NOT NULL,
    table_name text NOT NULL,
    configuration json,
    comment text
);
 '   DROP TABLE hdb_catalog.event_triggers;
       hdb_catalog         heap    kabeer_smart_recall    false    9            �            1259    7037114    hdb_allowlist    TABLE     E   CREATE TABLE hdb_catalog.hdb_allowlist (
    collection_name text
);
 &   DROP TABLE hdb_catalog.hdb_allowlist;
       hdb_catalog         heap    kabeer_smart_recall    false    9            �            1259    7036993    hdb_check_constraint    VIEW     �  CREATE VIEW hdb_catalog.hdb_check_constraint AS
 SELECT (n.nspname)::text AS table_schema,
    (ct.relname)::text AS table_name,
    (r.conname)::text AS constraint_name,
    pg_get_constraintdef(r.oid, true) AS "check"
   FROM ((pg_constraint r
     JOIN pg_class ct ON ((r.conrelid = ct.oid)))
     JOIN pg_namespace n ON ((ct.relnamespace = n.oid)))
  WHERE (r.contype = 'c'::"char");
 ,   DROP VIEW hdb_catalog.hdb_check_constraint;
       hdb_catalog          kabeer_smart_recall    false    9            �            1259    7037127    hdb_computed_field    TABLE     �   CREATE TABLE hdb_catalog.hdb_computed_field (
    table_schema text NOT NULL,
    table_name text NOT NULL,
    computed_field_name text NOT NULL,
    definition jsonb NOT NULL,
    comment text
);
 +   DROP TABLE hdb_catalog.hdb_computed_field;
       hdb_catalog         heap    kabeer_smart_recall    false    9            �            1259    7037140    hdb_computed_field_function    VIEW     �  CREATE VIEW hdb_catalog.hdb_computed_field_function AS
 SELECT hdb_computed_field.table_schema,
    hdb_computed_field.table_name,
    hdb_computed_field.computed_field_name,
        CASE
            WHEN (((hdb_computed_field.definition -> 'function'::text) ->> 'name'::text) IS NULL) THEN (hdb_computed_field.definition ->> 'function'::text)
            ELSE ((hdb_computed_field.definition -> 'function'::text) ->> 'name'::text)
        END AS function_name,
        CASE
            WHEN (((hdb_computed_field.definition -> 'function'::text) ->> 'schema'::text) IS NULL) THEN 'public'::text
            ELSE ((hdb_computed_field.definition -> 'function'::text) ->> 'schema'::text)
        END AS function_schema
   FROM hdb_catalog.hdb_computed_field;
 3   DROP VIEW hdb_catalog.hdb_computed_field_function;
       hdb_catalog          kabeer_smart_recall    false    226    226    226    226    9            �            1259    7036988    hdb_foreign_key_constraint    VIEW     ~  CREATE VIEW hdb_catalog.hdb_foreign_key_constraint AS
 SELECT (q.table_schema)::text AS table_schema,
    (q.table_name)::text AS table_name,
    (q.constraint_name)::text AS constraint_name,
    (min(q.constraint_oid))::integer AS constraint_oid,
    min((q.ref_table_table_schema)::text) AS ref_table_table_schema,
    min((q.ref_table)::text) AS ref_table,
    json_object_agg(ac.attname, afc.attname) AS column_mapping,
    min((q.confupdtype)::text) AS on_update,
    min((q.confdeltype)::text) AS on_delete,
    json_agg(ac.attname) AS columns,
    json_agg(afc.attname) AS ref_columns
   FROM ((( SELECT ctn.nspname AS table_schema,
            ct.relname AS table_name,
            r.conrelid AS table_id,
            r.conname AS constraint_name,
            r.oid AS constraint_oid,
            cftn.nspname AS ref_table_table_schema,
            cft.relname AS ref_table,
            r.confrelid AS ref_table_id,
            r.confupdtype,
            r.confdeltype,
            unnest(r.conkey) AS column_id,
            unnest(r.confkey) AS ref_column_id
           FROM ((((pg_constraint r
             JOIN pg_class ct ON ((r.conrelid = ct.oid)))
             JOIN pg_namespace ctn ON ((ct.relnamespace = ctn.oid)))
             JOIN pg_class cft ON ((r.confrelid = cft.oid)))
             JOIN pg_namespace cftn ON ((cft.relnamespace = cftn.oid)))
          WHERE (r.contype = 'f'::"char")) q
     JOIN pg_attribute ac ON (((q.column_id = ac.attnum) AND (q.table_id = ac.attrelid))))
     JOIN pg_attribute afc ON (((q.ref_column_id = afc.attnum) AND (q.ref_table_id = afc.attrelid))))
  GROUP BY q.table_schema, q.table_name, q.constraint_name;
 2   DROP VIEW hdb_catalog.hdb_foreign_key_constraint;
       hdb_catalog          kabeer_smart_recall    false    9            �            1259    7037056    hdb_function    TABLE     �   CREATE TABLE hdb_catalog.hdb_function (
    function_schema text NOT NULL,
    function_name text NOT NULL,
    configuration jsonb DEFAULT '{}'::jsonb NOT NULL,
    is_system_defined boolean DEFAULT false
);
 %   DROP TABLE hdb_catalog.hdb_function;
       hdb_catalog         heap    kabeer_smart_recall    false    9            �            1259    7037066    hdb_function_agg    VIEW     X  CREATE VIEW hdb_catalog.hdb_function_agg AS
 SELECT (p.proname)::text AS function_name,
    (pn.nspname)::text AS function_schema,
    pd.description,
        CASE
            WHEN (p.provariadic = (0)::oid) THEN false
            ELSE true
        END AS has_variadic,
        CASE
            WHEN ((p.provolatile)::text = ('i'::character(1))::text) THEN 'IMMUTABLE'::text
            WHEN ((p.provolatile)::text = ('s'::character(1))::text) THEN 'STABLE'::text
            WHEN ((p.provolatile)::text = ('v'::character(1))::text) THEN 'VOLATILE'::text
            ELSE NULL::text
        END AS function_type,
    pg_get_functiondef(p.oid) AS function_definition,
    (rtn.nspname)::text AS return_type_schema,
    (rt.typname)::text AS return_type_name,
    (rt.typtype)::text AS return_type_type,
    p.proretset AS returns_set,
    ( SELECT COALESCE(json_agg(json_build_object('schema', q.schema, 'name', q.name, 'type', q.type)), '[]'::json) AS "coalesce"
           FROM ( SELECT pt.typname AS name,
                    pns.nspname AS schema,
                    pt.typtype AS type,
                    pat.ordinality
                   FROM ((unnest(COALESCE(p.proallargtypes, (p.proargtypes)::oid[])) WITH ORDINALITY pat(oid, ordinality)
                     LEFT JOIN pg_type pt ON ((pt.oid = pat.oid)))
                     LEFT JOIN pg_namespace pns ON ((pt.typnamespace = pns.oid)))
                  ORDER BY pat.ordinality) q) AS input_arg_types,
    to_json(COALESCE(p.proargnames, ARRAY[]::text[])) AS input_arg_names,
    p.pronargdefaults AS default_args,
    (p.oid)::integer AS function_oid
   FROM ((((pg_proc p
     JOIN pg_namespace pn ON ((pn.oid = p.pronamespace)))
     JOIN pg_type rt ON ((rt.oid = p.prorettype)))
     JOIN pg_namespace rtn ON ((rtn.oid = rt.typnamespace)))
     LEFT JOIN pg_description pd ON ((p.oid = pd.objoid)))
  WHERE (((pn.nspname)::text !~~ 'pg_%'::text) AND ((pn.nspname)::text <> ALL (ARRAY['information_schema'::text, 'hdb_catalog'::text, 'hdb_views'::text])) AND (NOT (EXISTS ( SELECT 1
           FROM pg_aggregate
          WHERE ((pg_aggregate.aggfnoid)::oid = p.oid)))));
 (   DROP VIEW hdb_catalog.hdb_function_agg;
       hdb_catalog          kabeer_smart_recall    false    9            �            1259    7037099    hdb_function_info_agg    VIEW       CREATE VIEW hdb_catalog.hdb_function_info_agg AS
 SELECT hdb_function_agg.function_name,
    hdb_function_agg.function_schema,
    row_to_json(( SELECT e.*::record AS e
           FROM ( SELECT hdb_function_agg.description,
                    hdb_function_agg.has_variadic,
                    hdb_function_agg.function_type,
                    hdb_function_agg.return_type_schema,
                    hdb_function_agg.return_type_name,
                    hdb_function_agg.return_type_type,
                    hdb_function_agg.returns_set,
                    hdb_function_agg.input_arg_types,
                    hdb_function_agg.input_arg_names,
                    hdb_function_agg.default_args,
                    (EXISTS ( SELECT 1
                           FROM information_schema.tables
                          WHERE (((tables.table_schema)::name = hdb_function_agg.return_type_schema) AND ((tables.table_name)::name = hdb_function_agg.return_type_name)))) AS returns_table) e)) AS function_info
   FROM hdb_catalog.hdb_function_agg;
 -   DROP VIEW hdb_catalog.hdb_function_info_agg;
       hdb_catalog          kabeer_smart_recall    false    218    218    218    218    218    218    218    218    218    218    218    218    9            �            1259    7036969    hdb_permission    TABLE     �  CREATE TABLE hdb_catalog.hdb_permission (
    table_schema name NOT NULL,
    table_name name NOT NULL,
    role_name text NOT NULL,
    perm_type text NOT NULL,
    perm_def jsonb NOT NULL,
    comment text,
    is_system_defined boolean DEFAULT false,
    CONSTRAINT hdb_permission_perm_type_check CHECK ((perm_type = ANY (ARRAY['insert'::text, 'select'::text, 'update'::text, 'delete'::text])))
);
 '   DROP TABLE hdb_catalog.hdb_permission;
       hdb_catalog         heap    kabeer_smart_recall    false    9            �            1259    7036984    hdb_permission_agg    VIEW     f  CREATE VIEW hdb_catalog.hdb_permission_agg AS
 SELECT hdb_permission.table_schema,
    hdb_permission.table_name,
    hdb_permission.role_name,
    json_object_agg(hdb_permission.perm_type, hdb_permission.perm_def) AS permissions
   FROM hdb_catalog.hdb_permission
  GROUP BY hdb_permission.table_schema, hdb_permission.table_name, hdb_permission.role_name;
 *   DROP VIEW hdb_catalog.hdb_permission_agg;
       hdb_catalog          kabeer_smart_recall    false    208    208    208    208    208    9            �            1259    7037003    hdb_primary_key    VIEW     �	  CREATE VIEW hdb_catalog.hdb_primary_key AS
 SELECT tc.table_schema,
    tc.table_name,
    tc.constraint_name,
    json_agg(constraint_column_usage.column_name) AS columns
   FROM (information_schema.table_constraints tc
     JOIN ( SELECT x.tblschema AS table_schema,
            x.tblname AS table_name,
            x.colname AS column_name,
            x.cstrname AS constraint_name
           FROM ( SELECT DISTINCT nr.nspname,
                    r.relname,
                    a.attname,
                    c.conname
                   FROM pg_namespace nr,
                    pg_class r,
                    pg_attribute a,
                    pg_depend d,
                    pg_namespace nc,
                    pg_constraint c
                  WHERE ((nr.oid = r.relnamespace) AND (r.oid = a.attrelid) AND (d.refclassid = ('pg_class'::regclass)::oid) AND (d.refobjid = r.oid) AND (d.refobjsubid = a.attnum) AND (d.classid = ('pg_constraint'::regclass)::oid) AND (d.objid = c.oid) AND (c.connamespace = nc.oid) AND (c.contype = 'c'::"char") AND (r.relkind = ANY (ARRAY['r'::"char", 'p'::"char"])) AND (NOT a.attisdropped))
                UNION ALL
                 SELECT nr.nspname,
                    r.relname,
                    a.attname,
                    c.conname
                   FROM pg_namespace nr,
                    pg_class r,
                    pg_attribute a,
                    pg_namespace nc,
                    pg_constraint c
                  WHERE ((nr.oid = r.relnamespace) AND (r.oid = a.attrelid) AND (nc.oid = c.connamespace) AND (r.oid =
                        CASE c.contype
                            WHEN 'f'::"char" THEN c.confrelid
                            ELSE c.conrelid
                        END) AND (a.attnum = ANY (
                        CASE c.contype
                            WHEN 'f'::"char" THEN c.confkey
                            ELSE c.conkey
                        END)) AND (NOT a.attisdropped) AND (c.contype = ANY (ARRAY['p'::"char", 'u'::"char", 'f'::"char"])) AND (r.relkind = ANY (ARRAY['r'::"char", 'p'::"char"])))) x(tblschema, tblname, colname, cstrname)) constraint_column_usage ON ((((tc.constraint_name)::text = (constraint_column_usage.constraint_name)::text) AND ((tc.table_schema)::text = (constraint_column_usage.table_schema)::text) AND ((tc.table_name)::text = (constraint_column_usage.table_name)::text))))
  WHERE ((tc.constraint_type)::text = 'PRIMARY KEY'::text)
  GROUP BY tc.table_schema, tc.table_name, tc.constraint_name;
 '   DROP VIEW hdb_catalog.hdb_primary_key;
       hdb_catalog          kabeer_smart_recall    false    9            �            1259    7037105    hdb_query_collection    TABLE     �   CREATE TABLE hdb_catalog.hdb_query_collection (
    collection_name text NOT NULL,
    collection_defn jsonb NOT NULL,
    comment text,
    is_system_defined boolean DEFAULT false
);
 -   DROP TABLE hdb_catalog.hdb_query_collection;
       hdb_catalog         heap    kabeer_smart_recall    false    9            �            1259    7036954    hdb_relationship    TABLE     f  CREATE TABLE hdb_catalog.hdb_relationship (
    table_schema name NOT NULL,
    table_name name NOT NULL,
    rel_name text NOT NULL,
    rel_type text,
    rel_def jsonb NOT NULL,
    comment text,
    is_system_defined boolean DEFAULT false,
    CONSTRAINT hdb_relationship_rel_type_check CHECK ((rel_type = ANY (ARRAY['object'::text, 'array'::text])))
);
 )   DROP TABLE hdb_catalog.hdb_relationship;
       hdb_catalog         heap    kabeer_smart_recall    false    9            �            1259    7037084    hdb_schema_update_event    TABLE     �   CREATE TABLE hdb_catalog.hdb_schema_update_event (
    instance_id uuid NOT NULL,
    occurred_at timestamp with time zone DEFAULT now() NOT NULL,
    invalidations json NOT NULL
);
 0   DROP TABLE hdb_catalog.hdb_schema_update_event;
       hdb_catalog         heap    kabeer_smart_recall    false    9            �            1259    7036944 	   hdb_table    TABLE     �   CREATE TABLE hdb_catalog.hdb_table (
    table_schema name NOT NULL,
    table_name name NOT NULL,
    configuration jsonb,
    is_system_defined boolean DEFAULT false,
    is_enum boolean DEFAULT false NOT NULL
);
 "   DROP TABLE hdb_catalog.hdb_table;
       hdb_catalog         heap    kabeer_smart_recall    false    9            �            1259    7037094    hdb_table_info_agg    VIEW     �  CREATE VIEW hdb_catalog.hdb_table_info_agg AS
 SELECT schema.nspname AS table_schema,
    "table".relname AS table_name,
    jsonb_build_object('oid', ("table".oid)::integer, 'columns', COALESCE(columns.info, '[]'::jsonb), 'primary_key', primary_key.info, 'unique_constraints', COALESCE(unique_constraints.info, '[]'::jsonb), 'foreign_keys', COALESCE(foreign_key_constraints.info, '[]'::jsonb), 'view_info',
        CASE "table".relkind
            WHEN 'v'::"char" THEN jsonb_build_object('is_updatable', ((pg_relation_is_updatable(("table".oid)::regclass, true) & 4) = 4), 'is_insertable', ((pg_relation_is_updatable(("table".oid)::regclass, true) & 8) = 8), 'is_deletable', ((pg_relation_is_updatable(("table".oid)::regclass, true) & 16) = 16))
            ELSE NULL::jsonb
        END, 'description', description.description) AS info
   FROM ((((((pg_class "table"
     JOIN pg_namespace schema ON ((schema.oid = "table".relnamespace)))
     LEFT JOIN pg_description description ON (((description.classoid = ('pg_class'::regclass)::oid) AND (description.objoid = "table".oid) AND (description.objsubid = 0))))
     LEFT JOIN LATERAL ( SELECT jsonb_agg(jsonb_build_object('name', "column".attname, 'position', "column".attnum, 'type', COALESCE(base_type.typname, type.typname), 'is_nullable', (NOT "column".attnotnull), 'description', col_description("table".oid, ("column".attnum)::integer))) AS info
           FROM ((pg_attribute "column"
             LEFT JOIN pg_type type ON ((type.oid = "column".atttypid)))
             LEFT JOIN pg_type base_type ON (((type.typtype = 'd'::"char") AND (base_type.oid = type.typbasetype))))
          WHERE (("column".attrelid = "table".oid) AND ("column".attnum > 0) AND (NOT "column".attisdropped))) columns ON (true))
     LEFT JOIN LATERAL ( SELECT jsonb_build_object('constraint', jsonb_build_object('name', class.relname, 'oid', (class.oid)::integer), 'columns', COALESCE(columns_1.info, '[]'::jsonb)) AS info
           FROM ((pg_index index
             JOIN pg_class class ON ((class.oid = index.indexrelid)))
             LEFT JOIN LATERAL ( SELECT jsonb_agg("column".attname) AS info
                   FROM pg_attribute "column"
                  WHERE (("column".attrelid = "table".oid) AND ("column".attnum = ANY ((index.indkey)::smallint[])))) columns_1 ON (true))
          WHERE ((index.indrelid = "table".oid) AND index.indisprimary)) primary_key ON (true))
     LEFT JOIN LATERAL ( SELECT jsonb_agg(jsonb_build_object('name', class.relname, 'oid', (class.oid)::integer)) AS info
           FROM (pg_index index
             JOIN pg_class class ON ((class.oid = index.indexrelid)))
          WHERE ((index.indrelid = "table".oid) AND index.indisunique AND (NOT index.indisprimary))) unique_constraints ON (true))
     LEFT JOIN LATERAL ( SELECT jsonb_agg(jsonb_build_object('constraint', jsonb_build_object('name', foreign_key.constraint_name, 'oid', foreign_key.constraint_oid), 'columns', foreign_key.columns, 'foreign_table', jsonb_build_object('schema', foreign_key.ref_table_table_schema, 'name', foreign_key.ref_table), 'foreign_columns', foreign_key.ref_columns)) AS info
           FROM hdb_catalog.hdb_foreign_key_constraint foreign_key
          WHERE ((foreign_key.table_schema = schema.nspname) AND (foreign_key.table_name = "table".relname))) foreign_key_constraints ON (true))
  WHERE ("table".relkind = ANY (ARRAY['r'::"char", 't'::"char", 'v'::"char", 'm'::"char", 'f'::"char", 'p'::"char"]));
 *   DROP VIEW hdb_catalog.hdb_table_info_agg;
       hdb_catalog          kabeer_smart_recall    false    210    210    210    210    210    210    210    210    9            �            1259    7036998    hdb_unique_constraint    VIEW     �  CREATE VIEW hdb_catalog.hdb_unique_constraint AS
 SELECT tc.table_name,
    tc.constraint_schema AS table_schema,
    tc.constraint_name,
    json_agg(kcu.column_name) AS columns
   FROM (information_schema.table_constraints tc
     JOIN information_schema.key_column_usage kcu USING (constraint_schema, constraint_name))
  WHERE ((tc.constraint_type)::text = 'UNIQUE'::text)
  GROUP BY tc.table_name, tc.constraint_schema, tc.constraint_name;
 -   DROP VIEW hdb_catalog.hdb_unique_constraint;
       hdb_catalog          kabeer_smart_recall    false    9            �            1259    7036932    hdb_version    TABLE       CREATE TABLE hdb_catalog.hdb_version (
    hasura_uuid uuid DEFAULT public.gen_random_uuid() NOT NULL,
    version text NOT NULL,
    upgraded_on timestamp with time zone NOT NULL,
    cli_state jsonb DEFAULT '{}'::jsonb NOT NULL,
    console_state jsonb DEFAULT '{}'::jsonb NOT NULL
);
 $   DROP TABLE hdb_catalog.hdb_version;
       hdb_catalog         heap    kabeer_smart_recall    false    2    9            �            1259    7037073    remote_schemas    TABLE     z   CREATE TABLE hdb_catalog.remote_schemas (
    id bigint NOT NULL,
    name text,
    definition json,
    comment text
);
 '   DROP TABLE hdb_catalog.remote_schemas;
       hdb_catalog         heap    kabeer_smart_recall    false    9            �            1259    7037071    remote_schemas_id_seq    SEQUENCE     �   CREATE SEQUENCE hdb_catalog.remote_schemas_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 1   DROP SEQUENCE hdb_catalog.remote_schemas_id_seq;
       hdb_catalog          kabeer_smart_recall    false    220    9            =           0    0    remote_schemas_id_seq    SEQUENCE OWNED BY     Y   ALTER SEQUENCE hdb_catalog.remote_schemas_id_seq OWNED BY hdb_catalog.remote_schemas.id;
          hdb_catalog          kabeer_smart_recall    false    219            �            1259    7051036 
   categories    TABLE     T   CREATE TABLE public.categories (
    id integer NOT NULL,
    name text NOT NULL
);
    DROP TABLE public.categories;
       public         heap    kabeer_smart_recall    false            �            1259    7051034    categories_id_seq    SEQUENCE     �   CREATE SEQUENCE public.categories_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE public.categories_id_seq;
       public          kabeer_smart_recall    false    235            >           0    0    categories_id_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE public.categories_id_seq OWNED BY public.categories.id;
          public          kabeer_smart_recall    false    234            �            1259    7164427    task_priorities    TABLE     Y   CREATE TABLE public.task_priorities (
    id integer NOT NULL,
    name text NOT NULL
);
 #   DROP TABLE public.task_priorities;
       public         heap    kabeer_smart_recall    false            �            1259    7164425    task_priorities_id_seq    SEQUENCE     �   CREATE SEQUENCE public.task_priorities_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE public.task_priorities_id_seq;
       public          kabeer_smart_recall    false    239            ?           0    0    task_priorities_id_seq    SEQUENCE OWNED BY     Q   ALTER SEQUENCE public.task_priorities_id_seq OWNED BY public.task_priorities.id;
          public          kabeer_smart_recall    false    238            �            1259    7144963    task_statuses    TABLE     W   CREATE TABLE public.task_statuses (
    id integer NOT NULL,
    name text NOT NULL
);
 !   DROP TABLE public.task_statuses;
       public         heap    kabeer_smart_recall    false            �            1259    7144961    task_statuses_id_seq    SEQUENCE     �   CREATE SEQUENCE public.task_statuses_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 +   DROP SEQUENCE public.task_statuses_id_seq;
       public          kabeer_smart_recall    false    237            @           0    0    task_statuses_id_seq    SEQUENCE OWNED BY     M   ALTER SEQUENCE public.task_statuses_id_seq OWNED BY public.task_statuses.id;
          public          kabeer_smart_recall    false    236            �            1259    7051023    tasks    TABLE     �  CREATE TABLE public.tasks (
    id integer NOT NULL,
    name text NOT NULL,
    status_id integer NOT NULL,
    priority_id integer NOT NULL,
    category_id integer NOT NULL,
    due_date date DEFAULT now() NOT NULL,
    reminder_at timestamp with time zone DEFAULT now() NOT NULL,
    assigned_to integer NOT NULL,
    created_by integer NOT NULL,
    created_at timestamp with time zone NOT NULL,
    last_modified_at timestamp with time zone NOT NULL
);
    DROP TABLE public.tasks;
       public         heap    kabeer_smart_recall    false            �            1259    7051021    tasks_id_seq    SEQUENCE     �   CREATE SEQUENCE public.tasks_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 #   DROP SEQUENCE public.tasks_id_seq;
       public          kabeer_smart_recall    false    233            A           0    0    tasks_id_seq    SEQUENCE OWNED BY     =   ALTER SEQUENCE public.tasks_id_seq OWNED BY public.tasks.id;
          public          kabeer_smart_recall    false    232            �            1259    7048513    users    TABLE     �  CREATE TABLE public.users (
    id integer NOT NULL,
    email text NOT NULL,
    password text NOT NULL,
    firstname text NOT NULL,
    lastname text NOT NULL,
    phone text,
    last_login_time timestamp with time zone,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    last_updated_at timestamp with time zone DEFAULT now() NOT NULL,
    status_id integer DEFAULT 1 NOT NULL
);
    DROP TABLE public.users;
       public         heap    kabeer_smart_recall    false            �            1259    7048511    user_id_seq    SEQUENCE     �   CREATE SEQUENCE public.user_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 "   DROP SEQUENCE public.user_id_seq;
       public          kabeer_smart_recall    false    229            B           0    0    user_id_seq    SEQUENCE OWNED BY     <   ALTER SEQUENCE public.user_id_seq OWNED BY public.users.id;
          public          kabeer_smart_recall    false    228            �            1259    7048532    user_statuses    TABLE     W   CREATE TABLE public.user_statuses (
    ud integer NOT NULL,
    name text NOT NULL
);
 !   DROP TABLE public.user_statuses;
       public         heap    kabeer_smart_recall    false            �            1259    7048530    user_status_ud_seq    SEQUENCE     �   CREATE SEQUENCE public.user_status_ud_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 )   DROP SEQUENCE public.user_status_ud_seq;
       public          kabeer_smart_recall    false    231            C           0    0    user_status_ud_seq    SEQUENCE OWNED BY     K   ALTER SEQUENCE public.user_status_ud_seq OWNED BY public.user_statuses.ud;
          public          kabeer_smart_recall    false    230            A           2604    7037076    remote_schemas id    DEFAULT     �   ALTER TABLE ONLY hdb_catalog.remote_schemas ALTER COLUMN id SET DEFAULT nextval('hdb_catalog.remote_schemas_id_seq'::regclass);
 E   ALTER TABLE hdb_catalog.remote_schemas ALTER COLUMN id DROP DEFAULT;
       hdb_catalog          kabeer_smart_recall    false    219    220    220            L           2604    7051039    categories id    DEFAULT     n   ALTER TABLE ONLY public.categories ALTER COLUMN id SET DEFAULT nextval('public.categories_id_seq'::regclass);
 <   ALTER TABLE public.categories ALTER COLUMN id DROP DEFAULT;
       public          kabeer_smart_recall    false    234    235    235            N           2604    7164430    task_priorities id    DEFAULT     x   ALTER TABLE ONLY public.task_priorities ALTER COLUMN id SET DEFAULT nextval('public.task_priorities_id_seq'::regclass);
 A   ALTER TABLE public.task_priorities ALTER COLUMN id DROP DEFAULT;
       public          kabeer_smart_recall    false    238    239    239            M           2604    7144966    task_statuses id    DEFAULT     t   ALTER TABLE ONLY public.task_statuses ALTER COLUMN id SET DEFAULT nextval('public.task_statuses_id_seq'::regclass);
 ?   ALTER TABLE public.task_statuses ALTER COLUMN id DROP DEFAULT;
       public          kabeer_smart_recall    false    236    237    237            I           2604    7051026    tasks id    DEFAULT     d   ALTER TABLE ONLY public.tasks ALTER COLUMN id SET DEFAULT nextval('public.tasks_id_seq'::regclass);
 7   ALTER TABLE public.tasks ALTER COLUMN id DROP DEFAULT;
       public          kabeer_smart_recall    false    232    233    233            H           2604    7048535    user_statuses ud    DEFAULT     r   ALTER TABLE ONLY public.user_statuses ALTER COLUMN ud SET DEFAULT nextval('public.user_status_ud_seq'::regclass);
 ?   ALTER TABLE public.user_statuses ALTER COLUMN ud DROP DEFAULT;
       public          kabeer_smart_recall    false    231    230    231            D           2604    7048516    users id    DEFAULT     c   ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public.user_id_seq'::regclass);
 7   ALTER TABLE public.users ALTER COLUMN id DROP DEFAULT;
       public          kabeer_smart_recall    false    229    228    229                      0    7037040    event_invocation_logs 
   TABLE DATA           i   COPY hdb_catalog.event_invocation_logs (id, event_id, status, request, response, created_at) FROM stdin;
    hdb_catalog          kabeer_smart_recall    false    216   <�                 0    7037022 	   event_log 
   TABLE DATA           �   COPY hdb_catalog.event_log (id, schema_name, table_name, trigger_name, payload, delivered, error, tries, created_at, locked, next_retry_at, archived) FROM stdin;
    hdb_catalog          kabeer_smart_recall    false    215   Y�                 0    7037009    event_triggers 
   TABLE DATA           j   COPY hdb_catalog.event_triggers (name, type, schema_name, table_name, configuration, comment) FROM stdin;
    hdb_catalog          kabeer_smart_recall    false    214   v�       %          0    7037114    hdb_allowlist 
   TABLE DATA           =   COPY hdb_catalog.hdb_allowlist (collection_name) FROM stdin;
    hdb_catalog          kabeer_smart_recall    false    225   ��       &          0    7037127    hdb_computed_field 
   TABLE DATA           u   COPY hdb_catalog.hdb_computed_field (table_schema, table_name, computed_field_name, definition, comment) FROM stdin;
    hdb_catalog          kabeer_smart_recall    false    226   ��                  0    7037056    hdb_function 
   TABLE DATA           m   COPY hdb_catalog.hdb_function (function_schema, function_name, configuration, is_system_defined) FROM stdin;
    hdb_catalog          kabeer_smart_recall    false    217   ��                 0    7036969    hdb_permission 
   TABLE DATA           �   COPY hdb_catalog.hdb_permission (table_schema, table_name, role_name, perm_type, perm_def, comment, is_system_defined) FROM stdin;
    hdb_catalog          kabeer_smart_recall    false    208   ��       $          0    7037105    hdb_query_collection 
   TABLE DATA           q   COPY hdb_catalog.hdb_query_collection (collection_name, collection_defn, comment, is_system_defined) FROM stdin;
    hdb_catalog          kabeer_smart_recall    false    224   �                 0    7036954    hdb_relationship 
   TABLE DATA           �   COPY hdb_catalog.hdb_relationship (table_schema, table_name, rel_name, rel_type, rel_def, comment, is_system_defined) FROM stdin;
    hdb_catalog          kabeer_smart_recall    false    207   $�       #          0    7037084    hdb_schema_update_event 
   TABLE DATA           _   COPY hdb_catalog.hdb_schema_update_event (instance_id, occurred_at, invalidations) FROM stdin;
    hdb_catalog          kabeer_smart_recall    false    221   f�                 0    7036944 	   hdb_table 
   TABLE DATA           m   COPY hdb_catalog.hdb_table (table_schema, table_name, configuration, is_system_defined, is_enum) FROM stdin;
    hdb_catalog          kabeer_smart_recall    false    206   ��                 0    7036932    hdb_version 
   TABLE DATA           g   COPY hdb_catalog.hdb_version (hasura_uuid, version, upgraded_on, cli_state, console_state) FROM stdin;
    hdb_catalog          kabeer_smart_recall    false    205   %�       "          0    7037073    remote_schemas 
   TABLE DATA           L   COPY hdb_catalog.remote_schemas (id, name, definition, comment) FROM stdin;
    hdb_catalog          kabeer_smart_recall    false    220   ��       .          0    7051036 
   categories 
   TABLE DATA           .   COPY public.categories (id, name) FROM stdin;
    public          kabeer_smart_recall    false    235   ��       2          0    7164427    task_priorities 
   TABLE DATA           3   COPY public.task_priorities (id, name) FROM stdin;
    public          kabeer_smart_recall    false    239   6�       0          0    7144963    task_statuses 
   TABLE DATA           1   COPY public.task_statuses (id, name) FROM stdin;
    public          kabeer_smart_recall    false    237   ��       ,          0    7051023    tasks 
   TABLE DATA           �   COPY public.tasks (id, name, status_id, priority_id, category_id, due_date, reminder_at, assigned_to, created_by, created_at, last_modified_at) FROM stdin;
    public          kabeer_smart_recall    false    233   ��       *          0    7048532    user_statuses 
   TABLE DATA           1   COPY public.user_statuses (ud, name) FROM stdin;
    public          kabeer_smart_recall    false    231   �       (          0    7048513    users 
   TABLE DATA           �   COPY public.users (id, email, password, firstname, lastname, phone, last_login_time, created_at, last_updated_at, status_id) FROM stdin;
    public          kabeer_smart_recall    false    229   W�       D           0    0    remote_schemas_id_seq    SEQUENCE SET     I   SELECT pg_catalog.setval('hdb_catalog.remote_schemas_id_seq', 1, false);
          hdb_catalog          kabeer_smart_recall    false    219            E           0    0    categories_id_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('public.categories_id_seq', 9, true);
          public          kabeer_smart_recall    false    234            F           0    0    task_priorities_id_seq    SEQUENCE SET     D   SELECT pg_catalog.setval('public.task_priorities_id_seq', 6, true);
          public          kabeer_smart_recall    false    238            G           0    0    task_statuses_id_seq    SEQUENCE SET     B   SELECT pg_catalog.setval('public.task_statuses_id_seq', 9, true);
          public          kabeer_smart_recall    false    236            H           0    0    tasks_id_seq    SEQUENCE SET     ;   SELECT pg_catalog.setval('public.tasks_id_seq', 16, true);
          public          kabeer_smart_recall    false    232            I           0    0    user_id_seq    SEQUENCE SET     9   SELECT pg_catalog.setval('public.user_id_seq', 1, true);
          public          kabeer_smart_recall    false    228            J           0    0    user_status_ud_seq    SEQUENCE SET     @   SELECT pg_catalog.setval('public.user_status_ud_seq', 6, true);
          public          kabeer_smart_recall    false    230            a           2606    7037049 0   event_invocation_logs event_invocation_logs_pkey 
   CONSTRAINT     s   ALTER TABLE ONLY hdb_catalog.event_invocation_logs
    ADD CONSTRAINT event_invocation_logs_pkey PRIMARY KEY (id);
 _   ALTER TABLE ONLY hdb_catalog.event_invocation_logs DROP CONSTRAINT event_invocation_logs_pkey;
       hdb_catalog            kabeer_smart_recall    false    216            ]           2606    7037036    event_log event_log_pkey 
   CONSTRAINT     [   ALTER TABLE ONLY hdb_catalog.event_log
    ADD CONSTRAINT event_log_pkey PRIMARY KEY (id);
 G   ALTER TABLE ONLY hdb_catalog.event_log DROP CONSTRAINT event_log_pkey;
       hdb_catalog            kabeer_smart_recall    false    215            Y           2606    7037016 "   event_triggers event_triggers_pkey 
   CONSTRAINT     g   ALTER TABLE ONLY hdb_catalog.event_triggers
    ADD CONSTRAINT event_triggers_pkey PRIMARY KEY (name);
 Q   ALTER TABLE ONLY hdb_catalog.event_triggers DROP CONSTRAINT event_triggers_pkey;
       hdb_catalog            kabeer_smart_recall    false    214            l           2606    7037121 /   hdb_allowlist hdb_allowlist_collection_name_key 
   CONSTRAINT     z   ALTER TABLE ONLY hdb_catalog.hdb_allowlist
    ADD CONSTRAINT hdb_allowlist_collection_name_key UNIQUE (collection_name);
 ^   ALTER TABLE ONLY hdb_catalog.hdb_allowlist DROP CONSTRAINT hdb_allowlist_collection_name_key;
       hdb_catalog            kabeer_smart_recall    false    225            n           2606    7037134 *   hdb_computed_field hdb_computed_field_pkey 
   CONSTRAINT     �   ALTER TABLE ONLY hdb_catalog.hdb_computed_field
    ADD CONSTRAINT hdb_computed_field_pkey PRIMARY KEY (table_schema, table_name, computed_field_name);
 Y   ALTER TABLE ONLY hdb_catalog.hdb_computed_field DROP CONSTRAINT hdb_computed_field_pkey;
       hdb_catalog            kabeer_smart_recall    false    226    226    226            c           2606    7037065    hdb_function hdb_function_pkey 
   CONSTRAINT     }   ALTER TABLE ONLY hdb_catalog.hdb_function
    ADD CONSTRAINT hdb_function_pkey PRIMARY KEY (function_schema, function_name);
 M   ALTER TABLE ONLY hdb_catalog.hdb_function DROP CONSTRAINT hdb_function_pkey;
       hdb_catalog            kabeer_smart_recall    false    217    217            W           2606    7036978 "   hdb_permission hdb_permission_pkey 
   CONSTRAINT     �   ALTER TABLE ONLY hdb_catalog.hdb_permission
    ADD CONSTRAINT hdb_permission_pkey PRIMARY KEY (table_schema, table_name, role_name, perm_type);
 Q   ALTER TABLE ONLY hdb_catalog.hdb_permission DROP CONSTRAINT hdb_permission_pkey;
       hdb_catalog            kabeer_smart_recall    false    208    208    208    208            j           2606    7037113 .   hdb_query_collection hdb_query_collection_pkey 
   CONSTRAINT     ~   ALTER TABLE ONLY hdb_catalog.hdb_query_collection
    ADD CONSTRAINT hdb_query_collection_pkey PRIMARY KEY (collection_name);
 ]   ALTER TABLE ONLY hdb_catalog.hdb_query_collection DROP CONSTRAINT hdb_query_collection_pkey;
       hdb_catalog            kabeer_smart_recall    false    224            U           2606    7036963 &   hdb_relationship hdb_relationship_pkey 
   CONSTRAINT     �   ALTER TABLE ONLY hdb_catalog.hdb_relationship
    ADD CONSTRAINT hdb_relationship_pkey PRIMARY KEY (table_schema, table_name, rel_name);
 U   ALTER TABLE ONLY hdb_catalog.hdb_relationship DROP CONSTRAINT hdb_relationship_pkey;
       hdb_catalog            kabeer_smart_recall    false    207    207    207            S           2606    7036953    hdb_table hdb_table_pkey 
   CONSTRAINT     q   ALTER TABLE ONLY hdb_catalog.hdb_table
    ADD CONSTRAINT hdb_table_pkey PRIMARY KEY (table_schema, table_name);
 G   ALTER TABLE ONLY hdb_catalog.hdb_table DROP CONSTRAINT hdb_table_pkey;
       hdb_catalog            kabeer_smart_recall    false    206    206            Q           2606    7036942    hdb_version hdb_version_pkey 
   CONSTRAINT     h   ALTER TABLE ONLY hdb_catalog.hdb_version
    ADD CONSTRAINT hdb_version_pkey PRIMARY KEY (hasura_uuid);
 K   ALTER TABLE ONLY hdb_catalog.hdb_version DROP CONSTRAINT hdb_version_pkey;
       hdb_catalog            kabeer_smart_recall    false    205            e           2606    7037083 &   remote_schemas remote_schemas_name_key 
   CONSTRAINT     f   ALTER TABLE ONLY hdb_catalog.remote_schemas
    ADD CONSTRAINT remote_schemas_name_key UNIQUE (name);
 U   ALTER TABLE ONLY hdb_catalog.remote_schemas DROP CONSTRAINT remote_schemas_name_key;
       hdb_catalog            kabeer_smart_recall    false    220            g           2606    7037081 "   remote_schemas remote_schemas_pkey 
   CONSTRAINT     e   ALTER TABLE ONLY hdb_catalog.remote_schemas
    ADD CONSTRAINT remote_schemas_pkey PRIMARY KEY (id);
 Q   ALTER TABLE ONLY hdb_catalog.remote_schemas DROP CONSTRAINT remote_schemas_pkey;
       hdb_catalog            kabeer_smart_recall    false    220            z           2606    7051046    categories categories_name_key 
   CONSTRAINT     Y   ALTER TABLE ONLY public.categories
    ADD CONSTRAINT categories_name_key UNIQUE (name);
 H   ALTER TABLE ONLY public.categories DROP CONSTRAINT categories_name_key;
       public            kabeer_smart_recall    false    235            |           2606    7051044    categories categories_pkey 
   CONSTRAINT     X   ALTER TABLE ONLY public.categories
    ADD CONSTRAINT categories_pkey PRIMARY KEY (id);
 D   ALTER TABLE ONLY public.categories DROP CONSTRAINT categories_pkey;
       public            kabeer_smart_recall    false    235            �           2606    7164441 (   task_priorities task_priorities_name_key 
   CONSTRAINT     c   ALTER TABLE ONLY public.task_priorities
    ADD CONSTRAINT task_priorities_name_key UNIQUE (name);
 R   ALTER TABLE ONLY public.task_priorities DROP CONSTRAINT task_priorities_name_key;
       public            kabeer_smart_recall    false    239            �           2606    7164436 $   task_priorities task_priorities_pkey 
   CONSTRAINT     b   ALTER TABLE ONLY public.task_priorities
    ADD CONSTRAINT task_priorities_pkey PRIMARY KEY (id);
 N   ALTER TABLE ONLY public.task_priorities DROP CONSTRAINT task_priorities_pkey;
       public            kabeer_smart_recall    false    239            ~           2606    7144978 $   task_statuses task_statuses_name_key 
   CONSTRAINT     _   ALTER TABLE ONLY public.task_statuses
    ADD CONSTRAINT task_statuses_name_key UNIQUE (name);
 N   ALTER TABLE ONLY public.task_statuses DROP CONSTRAINT task_statuses_name_key;
       public            kabeer_smart_recall    false    237            �           2606    7144974     task_statuses task_statuses_pkey 
   CONSTRAINT     ^   ALTER TABLE ONLY public.task_statuses
    ADD CONSTRAINT task_statuses_pkey PRIMARY KEY (id);
 J   ALTER TABLE ONLY public.task_statuses DROP CONSTRAINT task_statuses_pkey;
       public            kabeer_smart_recall    false    237            x           2606    7051033    tasks tasks_pkey 
   CONSTRAINT     N   ALTER TABLE ONLY public.tasks
    ADD CONSTRAINT tasks_pkey PRIMARY KEY (id);
 :   ALTER TABLE ONLY public.tasks DROP CONSTRAINT tasks_pkey;
       public            kabeer_smart_recall    false    233            p           2606    7048525    users user_email_key 
   CONSTRAINT     P   ALTER TABLE ONLY public.users
    ADD CONSTRAINT user_email_key UNIQUE (email);
 >   ALTER TABLE ONLY public.users DROP CONSTRAINT user_email_key;
       public            kabeer_smart_recall    false    229            r           2606    7048523    users user_pkey 
   CONSTRAINT     M   ALTER TABLE ONLY public.users
    ADD CONSTRAINT user_pkey PRIMARY KEY (id);
 9   ALTER TABLE ONLY public.users DROP CONSTRAINT user_pkey;
       public            kabeer_smart_recall    false    229            t           2606    7048542 "   user_statuses user_status_name_key 
   CONSTRAINT     ]   ALTER TABLE ONLY public.user_statuses
    ADD CONSTRAINT user_status_name_key UNIQUE (name);
 L   ALTER TABLE ONLY public.user_statuses DROP CONSTRAINT user_status_name_key;
       public            kabeer_smart_recall    false    231            v           2606    7048540    user_statuses user_status_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY public.user_statuses
    ADD CONSTRAINT user_status_pkey PRIMARY KEY (ud);
 H   ALTER TABLE ONLY public.user_statuses DROP CONSTRAINT user_status_pkey;
       public            kabeer_smart_recall    false    231            _           1259    7037055 "   event_invocation_logs_event_id_idx    INDEX     m   CREATE INDEX event_invocation_logs_event_id_idx ON hdb_catalog.event_invocation_logs USING btree (event_id);
 ;   DROP INDEX hdb_catalog.event_invocation_logs_event_id_idx;
       hdb_catalog            kabeer_smart_recall    false    216            Z           1259    7037039    event_log_delivered_idx    INDEX     W   CREATE INDEX event_log_delivered_idx ON hdb_catalog.event_log USING btree (delivered);
 0   DROP INDEX hdb_catalog.event_log_delivered_idx;
       hdb_catalog            kabeer_smart_recall    false    215            [           1259    7037038    event_log_locked_idx    INDEX     Q   CREATE INDEX event_log_locked_idx ON hdb_catalog.event_log USING btree (locked);
 -   DROP INDEX hdb_catalog.event_log_locked_idx;
       hdb_catalog            kabeer_smart_recall    false    215            ^           1259    7037037    event_log_trigger_name_idx    INDEX     ]   CREATE INDEX event_log_trigger_name_idx ON hdb_catalog.event_log USING btree (trigger_name);
 3   DROP INDEX hdb_catalog.event_log_trigger_name_idx;
       hdb_catalog            kabeer_smart_recall    false    215            h           1259    7037091    hdb_schema_update_event_one_row    INDEX     �   CREATE UNIQUE INDEX hdb_schema_update_event_one_row ON hdb_catalog.hdb_schema_update_event USING btree (((occurred_at IS NOT NULL)));
 8   DROP INDEX hdb_catalog.hdb_schema_update_event_one_row;
       hdb_catalog            kabeer_smart_recall    false    221    221            O           1259    7036943    hdb_version_one_row    INDEX     j   CREATE UNIQUE INDEX hdb_version_one_row ON hdb_catalog.hdb_version USING btree (((version IS NOT NULL)));
 ,   DROP INDEX hdb_catalog.hdb_version_one_row;
       hdb_catalog            kabeer_smart_recall    false    205    205            �           2620    7037093 8   hdb_schema_update_event hdb_schema_update_event_notifier    TRIGGER     �   CREATE TRIGGER hdb_schema_update_event_notifier AFTER INSERT OR UPDATE ON hdb_catalog.hdb_schema_update_event FOR EACH ROW EXECUTE FUNCTION hdb_catalog.hdb_schema_update_event_notifier();
 V   DROP TRIGGER hdb_schema_update_event_notifier ON hdb_catalog.hdb_schema_update_event;
       hdb_catalog          kabeer_smart_recall    false    277    221            �           2606    7037050 9   event_invocation_logs event_invocation_logs_event_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY hdb_catalog.event_invocation_logs
    ADD CONSTRAINT event_invocation_logs_event_id_fkey FOREIGN KEY (event_id) REFERENCES hdb_catalog.event_log(id);
 h   ALTER TABLE ONLY hdb_catalog.event_invocation_logs DROP CONSTRAINT event_invocation_logs_event_id_fkey;
       hdb_catalog          kabeer_smart_recall    false    215    3933    216            �           2606    7037017 9   event_triggers event_triggers_schema_name_table_name_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY hdb_catalog.event_triggers
    ADD CONSTRAINT event_triggers_schema_name_table_name_fkey FOREIGN KEY (schema_name, table_name) REFERENCES hdb_catalog.hdb_table(table_schema, table_name) ON UPDATE CASCADE;
 h   ALTER TABLE ONLY hdb_catalog.event_triggers DROP CONSTRAINT event_triggers_schema_name_table_name_fkey;
       hdb_catalog          kabeer_smart_recall    false    206    206    214    3923    214            �           2606    7037122 0   hdb_allowlist hdb_allowlist_collection_name_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY hdb_catalog.hdb_allowlist
    ADD CONSTRAINT hdb_allowlist_collection_name_fkey FOREIGN KEY (collection_name) REFERENCES hdb_catalog.hdb_query_collection(collection_name);
 _   ALTER TABLE ONLY hdb_catalog.hdb_allowlist DROP CONSTRAINT hdb_allowlist_collection_name_fkey;
       hdb_catalog          kabeer_smart_recall    false    3946    225    224            �           2606    7037135 B   hdb_computed_field hdb_computed_field_table_schema_table_name_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY hdb_catalog.hdb_computed_field
    ADD CONSTRAINT hdb_computed_field_table_schema_table_name_fkey FOREIGN KEY (table_schema, table_name) REFERENCES hdb_catalog.hdb_table(table_schema, table_name) ON UPDATE CASCADE;
 q   ALTER TABLE ONLY hdb_catalog.hdb_computed_field DROP CONSTRAINT hdb_computed_field_table_schema_table_name_fkey;
       hdb_catalog          kabeer_smart_recall    false    206    206    226    226    3923            �           2606    7036979 :   hdb_permission hdb_permission_table_schema_table_name_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY hdb_catalog.hdb_permission
    ADD CONSTRAINT hdb_permission_table_schema_table_name_fkey FOREIGN KEY (table_schema, table_name) REFERENCES hdb_catalog.hdb_table(table_schema, table_name) ON UPDATE CASCADE;
 i   ALTER TABLE ONLY hdb_catalog.hdb_permission DROP CONSTRAINT hdb_permission_table_schema_table_name_fkey;
       hdb_catalog          kabeer_smart_recall    false    206    208    208    206    3923            �           2606    7036964 >   hdb_relationship hdb_relationship_table_schema_table_name_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY hdb_catalog.hdb_relationship
    ADD CONSTRAINT hdb_relationship_table_schema_table_name_fkey FOREIGN KEY (table_schema, table_name) REFERENCES hdb_catalog.hdb_table(table_schema, table_name) ON UPDATE CASCADE;
 m   ALTER TABLE ONLY hdb_catalog.hdb_relationship DROP CONSTRAINT hdb_relationship_table_schema_table_name_fkey;
       hdb_catalog          kabeer_smart_recall    false    206    206    207    3923    207            �           2606    7186738    tasks task_assigned_to_fk    FK CONSTRAINT     �   ALTER TABLE ONLY public.tasks
    ADD CONSTRAINT task_assigned_to_fk FOREIGN KEY (assigned_to) REFERENCES public.users(id) NOT VALID;
 C   ALTER TABLE ONLY public.tasks DROP CONSTRAINT task_assigned_to_fk;
       public          kabeer_smart_recall    false    233    229    3954            �           2606    7186723    tasks task_category_fk    FK CONSTRAINT     �   ALTER TABLE ONLY public.tasks
    ADD CONSTRAINT task_category_fk FOREIGN KEY (category_id) REFERENCES public.categories(id) NOT VALID;
 @   ALTER TABLE ONLY public.tasks DROP CONSTRAINT task_category_fk;
       public          kabeer_smart_recall    false    233    235    3964            �           2606    7186743    tasks task_created_by_fk    FK CONSTRAINT     �   ALTER TABLE ONLY public.tasks
    ADD CONSTRAINT task_created_by_fk FOREIGN KEY (created_by) REFERENCES public.users(id) NOT VALID;
 B   ALTER TABLE ONLY public.tasks DROP CONSTRAINT task_created_by_fk;
       public          kabeer_smart_recall    false    233    3954    229            �           2606    7186733    tasks task_priority_fk    FK CONSTRAINT     �   ALTER TABLE ONLY public.tasks
    ADD CONSTRAINT task_priority_fk FOREIGN KEY (priority_id) REFERENCES public.task_priorities(id) NOT VALID;
 @   ALTER TABLE ONLY public.tasks DROP CONSTRAINT task_priority_fk;
       public          kabeer_smart_recall    false    233    239    3972            �           2606    7186728    tasks task_status_fk    FK CONSTRAINT     �   ALTER TABLE ONLY public.tasks
    ADD CONSTRAINT task_status_fk FOREIGN KEY (status_id) REFERENCES public.task_statuses(id) NOT VALID;
 >   ALTER TABLE ONLY public.tasks DROP CONSTRAINT task_status_fk;
       public          kabeer_smart_recall    false    233    3968    237            �           2606    7186753    users user_user_status    FK CONSTRAINT     �   ALTER TABLE ONLY public.users
    ADD CONSTRAINT user_user_status FOREIGN KEY (status_id) REFERENCES public.user_statuses(ud) NOT VALID;
 @   ALTER TABLE ONLY public.users DROP CONSTRAINT user_user_status;
       public          kabeer_smart_recall    false    229    3958    231                  x������ � �            x������ � �            x������ � �      %      x������ � �      &      x������ � �             x������ � �            x������ � �      $      x������ � �         2  x�͖ώ� ���S�|^�zkz�Ǖ�cLc���J�*�^0N��u�6���A��|3ںB�IZ���ઃ��YW��S���;D�h�
&E���T���5N��K9�����Ԥ���T|F���}@dg�@�=t���(a���e4S���|.^���.���X�C�9�P��(��T��J��.gaous��Q�ۻ�Fa&L�Q�^�
�шnY�k5f��`�3���L{3	aJwc�H�[5jtu�ɔ��-����k�n�Z�~Z�@� ��Uxa�Q�RP:s<����ߎ������m�6�\O��j�j]��x�@B��5�L�JNgO�";
��u�W�0;/�{r#��j� f�a���X%BE��n���������M��۪c�p9�J�@�����SO!�Ua���\���#����1�4%\�6V?LNH�"F_�5l��d�pΘڊR���^�����]s�����H�S 2r�`#1Z�5ޥ��I�$Lozn?��A��	�#I�x����Ve���	�ÿ�OE�>	z�t8~�e֟      #   l   x��A� �u9�a��|�3(\E��0�1iqe���)��&��@IT���lag��jX��`�F)K�/��L_�m�VF�yl;���{�c�/�e��z������\I         3  x����n� ���ST=O{�=�$D�C������I��S➂|��0���f�A%3����A��g�G�(FV#�������x9}���*h���q7����>�e�+�Ux�m��t�)��h���t
�B��f�	R�@*�L	�Ic`��&:M�H� �R͠�Vl�_2åA$���9�s���lX!�bBk��j�AkK��a��Ut�)Y�c�r��<�>2|�f��%�ї�J��q��j���a:R K�.'����7E%֜����u�0E�H����?>)��(�]m���g|���k����={>         t   x���
�0 �s�cW�H��f�/^�X�`A7�������� 
<�L�@�R�$ʁ9��#����$q��1�|B4�a���W}׾�.ko������\�˘��}�ao����I]      "      x������ � �      .   `   x�3����M�2��/��2�N�����2�t*-��K-.�2��IM,���K�2�t��K�KN�2�tK��̩��J�I,�,K-���IL2b���� �^      2   A   x�3�����/��2�t.�,�LN��2���L��2��MM�,��2���/�2�)�,����qqq ��Y      0   h   x�3�����/��2��/H��2���S�I����K�2��ӋR���L9]R�R��RS��8]��
�,sN��܂�� ے3<1��Q!-�H�3����+F��� o��      ,   �  x���M�� ��ί8�rE�&�ݝ[:t�r)�n�qg"�h0m�}�t�Bi�$�`�������[�@{y�0Oz;j�g��C#�<����@T�yߴ�a����r��u��w����C/+�9�G�(��r��z�z����.�P������t��#rt�/pJ�������!�KѴ�(��6oG}���
��c�R`��؛��M�9W��kL�1������Im{�q�$��.��"��e�+�Җ�R�D�bz���s������na�!���E'����ЫN�7�l��c>�]4j�����(�EGTe}Q��s��_��3��lK�u�g-��e����"�)�ɦ�@�?w@���Ղ˔��[��Sv�k�#�C��'���Lq�*mJ{M^�^�D_j.;og8/pKֆApYjN�j��v	�4�:��v�UO8��tDV�z_��汔}����^����n[W����B���Zfw�l�[=T+��R�w|8~}3��      *   D   x�3�����/��2�tL.�,K�2���S���I�2�t�,NL�IM�2�t�(�,��8=SrR�b���� ?�g      (   c   x���A@0 ������nQ'�/�h	��8��ac�W���z:<�~��@��g� �c�b!�02�cFؘ���p%.G�S�R�U	T     