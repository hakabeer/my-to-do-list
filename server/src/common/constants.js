var HttpStatus = require('http-status-codes');

const contstansts = {

    navigation: {
        MAX_ITEMS_PER_PAGE: 10,
        SHOW_NAVIGATION: true
    },

    user: {
        DEFAULT_SUPER_ADMIN_USER_ID: 1
    },
    auth: {
        SECRET_KEY: "thisisthesimplesecretkey",
        TOKEN_VALIDITY_SEVEN_DAYS: 7,
        TOKEN_VALIDITY_ONE_MONTH: 30
    },
    http_status: HttpStatus,

}

module.exports = contstansts;
