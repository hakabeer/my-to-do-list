/*jslint node: true */
"use strict";
const util = require('../../common/utilities');
const commonList = require('../../repositories/common/common_lists');
const logger = require("../../../logger");
var log = logger.LOG;

var common_list_module = {
    getAllCategories: function (req, res) {
        commonList.getAllCategories(function (error, result) {
            util.getListAPIResponse(res, error, result);
        });
    },

    getAllTaskPriorities: function (req, res) {
        commonList.getAllTaskPriorities(function (error, result) {
            util.getListAPIResponse(res, error, result);
        });
    },

    getAllTaskStatuses: function (req, res) {
        commonList.getAllTaskStatuses(function (error, result) {
            util.getListAPIResponse(res, error, result);
        });
    },

    getAllUserStatuses: function (req, res) {
        commonList.getAllUserStatuses(function (error, result) {
            util.getListAPIResponse(res, error, result);
        });
    }
}

module.exports = common_list_module;
