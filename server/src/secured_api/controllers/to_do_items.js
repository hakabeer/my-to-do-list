/*jslint node: true */
"use strict";
var constants = require('../../common/constants');
var toDoRepo = require('../../repositories/to_do_items/to_do_items');
const logger = require("../../../logger");
var log = logger.LOG;

var to_do_module = {
    getToDoItems: function(req, res) {
        const payload = (req && req.body) ? req.body : {};
        const filter = payload.filter ? payload.filter : {};

        toDoRepo.getToDoItems(filter, function (error, result){
            if(error){
                const responseObj = {
                    "status": constants.http_status.INTERNAL_SERVER_ERROR,
                    "message": "Error in fetching To Do Items",
                    "data": null
                };
                res.status(constants.http_status.OK).json(responseObj);
            }
            if(result) {
                const data = (result && result.rows) ? result.rows : [];
                const responseObj = {
                    "status": constants.http_status.OK,
                    "message": "List fetched Successfully",
                    "data": data
                };
                res.status(constants.http_status.OK).json(responseObj);
            }
        })
    }

  }
module.exports = to_do_module;
