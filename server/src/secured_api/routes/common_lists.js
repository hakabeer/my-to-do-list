var ctrlCommonLists = require('../controllers/common_lists');

module.exports = function(router) {
    console.log('Common List APIs');
    router
        .route('/secured_api/common/list/categories')
        .get(ctrlCommonLists.getAllCategories);

    router
        .route('/secured_api/common/list/task_priorities')
        .get(ctrlCommonLists.getAllTaskPriorities);

    router
        .route('/secured_api/common/list/task_statuses')
        .get(ctrlCommonLists.getAllTaskStatuses);

    router
        .route('/secured_api/common/list/user_statuses')
        .get(ctrlCommonLists.getAllUserStatuses);

    return router;
}
