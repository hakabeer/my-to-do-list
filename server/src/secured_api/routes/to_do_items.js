var ctrlToDoItems = require('../controllers/to_do_items');

module.exports = function(router) {
    console.log('To Do APIs');
    router
        .route('/secured_api/to_do_items')
        .get(ctrlToDoItems.getToDoItems);
        router
        .route('/secured_api/to_do_items')
        .post(ctrlToDoItems.getToDoItems);
    return router;
}