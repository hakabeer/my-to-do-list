var router = require("express").Router();

require("./to_do_items")(router);
require("./common_lists")(router);
module.exports = router;
