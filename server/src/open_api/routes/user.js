//var ctrlUser = require('../controllers/authentication/user');
var ctrlAuth = require('../controllers/authentication/auth');

module.exports = function(router) {
    console.log('configure users routes (Open API)');
    router
        .route('/api/user/login')
        .post(ctrlAuth.login);
    return router;
}
