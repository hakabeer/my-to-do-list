/*jslint node: true */
"use strict";
//Reference: http://thejackalofjavascript.com/architecting-a-restful-node-js-app
var jwt = require('jwt-simple');
// var User = require("../../../models/config/user");
var secretKey = require('../../controllers/authentication/secret');
const util = require('../../../common/utilities');
var constants = require('../../../common/constants');
// const userSessionCache = require('../../../services/cache/cache_mgr').getUserSession();
var userRepo = require('../../../repositories/authentication/users');
const logger = require("../../../../logger");
var log = logger.LOG;

var auth = {
    login: function(req, res) {
        log.debug(`req: ${JSON.stringify(req.body)}`);
        var email = req.body.email || '';
        var password = req.body.password || '';
        log.debug(`req.body: ${JSON.stringify(req.body)}`);
        var timezone_offset = req.body.timezone_offset || 0;
        if (email === '' || password === '') {
            let msg = "Failed to authenticate. email or password cannot be null.";
            log.debug('ERROR! ' + msg);
            res.status(constants.http_status.OK);
            res.json({
                "status": constants.http_status.UNAUTHORIZED,
                "message": msg,
                "payload": null,
                "user_id": constants.user.DEFAULT_SUPER_ADMIN_USER_ID
            });
        } else {
            auth.validate(email, password, function(err, data) {
                if (err) { // If authentication fails, we send a 401 back
                    // get a user record again
                  log.error(`err: ${JSON.stringify(err)}`);
                  userRepo.findUserByEmail(email, function(error, users) {

                        if (error) {
                            // User name does not exist in the system!!!
                            let msg = "Failed to authenticate. Please check your email address and password.";
                            log.debug('ERROR: User name does not exist in the system!!!');
                            const content = {
                                "status": constants.http_status.UNAUTHORIZED,
                                "message": msg,
                                "payload": null
                            };
                            util.sendJSONresponse(res, constants.http_status.OK, content);
                        }

                        if (users){
                            log.debug('users ==> '+ JSON.stringify(users));
                                let msg = "Failed to authenticate. Please check your email address and password.";
                                log.debug(msg);
                                const content = {
                                    "status": constants.http_status.OK,
                                    "message": msg,
                                    "payload": null
                                };
                                util.sendJSONresponse(res, constants.http_status.OK, content);
                        }
                    });
                } else { // The user was autheticated successfully
                    // Does the user belong to this server. If not, redirect
                    // the login to the right server.
                    if (data.user) {
                        const jwt = generateJWT(data.user, timezone_offset);
                        const payload = util.decodeJWT(jwt);
                        payload.accessToken = jwt;
                        const key = util.createUserSessionKey(payload.user.id, payload.createdAt);
                        const userLoginInfo = {
                            "user": payload.user,
                            "token": payload.accessToken,
                            "createdAt": payload.createdAt
                        };
                        // userSessionCache.setSession(key, userLoginInfo);
                        // userSessionCache.printStatus();

                        // Returning the payload containing the JWT token to the
                        // client. All subsequence request must have this token
                        // attached to the request header.
                        res.status(constants.http_status.OK).json({
                            "status": constants.http_status.OK,
                            "message": null,
                            "payload": payload
                        });
                    }
                }
            });
        }
    },

    logout: function(req, res) {
        const key = util.getUserSessionKey(req);
        if (key) {
            log.debug('Logging out user: '+key);
            userSessionCache.removeSession(key);
            userSessionCache.printStatus();
            res.status(constants.http_status.OK).json({
                "status": "SUCCESS",
                "message": "Successfully logging out a user.",
                "payload": {}
            });
        }
    },

    validate: function(email, password, callback) {
        log.debug(`Authenticating user by  email: ${email}`);
      userRepo.authenticate(email, password, function(error, result) {
          if(error){
              const msg = `FAILED in authenticating user by email: ${email}`;
              log.debug(msg);
              callback({msg: msg}, null);
          }

          if(result){
              log.debug(`result (in 186=>auth.js): ${JSON.stringify(result)}`);
              const validUser = (result && result.length>0) || false;
              if (validUser === true) {
                  log.debug(`SUCCESS in authenticating user by email: ${email}`);
                  const user = result[0];
                  // const msp = user.user_type_id === constants.user.ADMIN_USER_TYPE_ID ||
                  //             user.user_type_id === constants.user.SUPER_USER_TYPE_ID;
                  // const redirect = isRedirect(hostname, user.redirected_server);
                  // if (!redirect) {
                  //     setUserLoggedInDetails(user, extractIPv4Field(remoteIP));
                  // }
                  callback(null, {
                      user: user
                  });
              }
          }
        })
    }

  }


// private method
function generateJWT(user, timezone_offset) {
    return jwt.encode({
        exp: expiresIn(constants.auth.TOKEN_VALIDITY_ONE_MONTH),
        user: user,
        createdAt: new Date().getTime()
    }, secretKey.getSecretKey());
}

function expiresIn(numDays) {
    var dateObj = new Date();
    return dateObj.setDate(dateObj.getDate() + numDays);
}

module.exports = auth;
