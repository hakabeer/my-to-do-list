const knex = require('knex');
//Reference: http://knexjs.org/
if (!process.env.SMARTRECALL_SERVER_ENV || process.env.SMARTRECALL_SERVER_ENV === 'local' || process.env.SMARTRECALL_SERVER_ENV === 'development') {
  require('dotenv').config({path:'.env'});
}

const unSecuredConnection = {
    host: process.env.PSQL_HOST,
    database: process.env.PSQL_DATABASE,
    user: process.env.PSQL_USER,
    password: process.env.PSQL_PASSWORD,
    port: process.env.PSQL_PORT
}

const securedConnection = {
    host: process.env.PSQL_HOST,
    database: process.env.PSQL_DATABASE,
    user: process.env.PSQL_USER,
    password: process.env.PSQL_PASSWORD,
    port: process.env.PSQL_PORT,
    ssl: process.env.PSQL_SSL_SUPPORT,
    dialect: 'postgres',
    dialectOptions: {
      "ssl": {"require": process.env.PSQL_SSL_SUPPORT}
    }
}

const psql_config = {
  client: 'pg',
  connection: (process.env.PSQL_SSL_SUPPORT=="true") ? securedConnection : unSecuredConnection,  
};

process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
const psql_client = knex(psql_config);

module.exports = psql_client;