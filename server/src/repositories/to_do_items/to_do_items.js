const psql_client  = require('../../db/postgres');
const logger = require("../../../logger");
const log = logger.LOG;

const toDoItemsContoller = {

  getToDoItems:  (filter, callback) => {

      let query = `SELECT 
                          tasks.id,
                          tasks.name,
                          task_priorities.name AS priority,
                          categories.name AS category,
                          CONCAT(users.firstname,' ',users.lastname) AS assignee,
                          tasks.due_date,
                          to_char(tasks.due_date, 'Dy Mon DD, YYYY') AS formatted_due_date,
                          tasks.reminder_at,
                          to_char(tasks.reminder_at, 'Dy Mon DD, YYYY HH:MI AM') AS formatted_reminder_at,
                          task_statuses.name AS status
                    FROM
                        public.tasks,
                        public.task_priorities,
                        public.task_statuses,
                        public.users,
                        public.categories 
                    WHERE
                        tasks.category_id = categories.id
                      AND
                        tasks.priority_id = task_priorities.id
                      AND
                        tasks.status_id = task_statuses.id
                      AND
                        tasks.assigned_to = users.id
                  `;

      query = toDoItemsContoller.appendIntegerQuery (query, `categories.id`, filter.task_category_id);
      query = toDoItemsContoller.appendIntegerQuery (query, `task_priorities.id`, filter.task_priority_id);
      query = toDoItemsContoller.appendIntegerQuery (query, `task_statuses.id`, filter.task_status_id);
      query = toDoItemsContoller.appendIntegerQuery (query, `users.id`, filter.user_id);

      //console.log(`query: ${query}`);
      const input_values = [];
      psql_client.raw(query, input_values)
        .then(function (result) {
          callback(null, result);
        }).catch(function (error) {
          log.error(`Error: ${error}`);
          callback(error, null);
      });

  },

  appendIntegerQuery: (query, column, value) => {
    return (query && column && value  && value != -1) ? (query + ` AND ${column} = ${value}`) : query;
  }

}

module.exports = toDoItemsContoller;