const psql_client  = require('../../db/postgres');
const logger = require("../../../logger");
const log = logger.LOG;

module.exports.authenticate  = function (email, password, callback) {

  if(email && password && email !== "" && password !== ""){

        const query = `SELECT
                            id,
                            email,
                            firstname,
                            lastname,
                            phone,
                            last_login_time,
                            status_id
                      FROM public.users
                       WHERE 
                          UPPER(email) = ? AND password = ?`;
        const input_values = [email.toUpperCase(), password];
        log.debug(`query: ${query}`);
        log.debug(`input_values: ${input_values}`);
        psql_client.raw(query, input_values)
          .then(function (data) {
            log.debug(`data: ${JSON.stringify(data)}`);
            const result = (data.rows) ? data.rows : [];
            callback(null, result);
          }).catch(function (error) {
            log.error(`Error: ${error}`);
            callback(error, null);
        });
  } else {
    const result = {
      rows: []
    }
    callback(null, result);
  }
};

module.exports.findUserByEmail  = function (email, callback) {
  if(email && email !== ""){
    const query = `SELECT
                        id,
                        email,
                        firstname,
                        lastname,
                        phone,
                        last_login_time,
                        status_id
                    FROM public.users
                    WHERE 
                      email = ?`;

    const input_values = [email];
    psql_client.raw(query, input_values)
      .then(function (data) {
        const result = (data.rows) ? data.rows : {};
        callback(null, result);
      }).catch(function (error) {
        log.error(`Error: ${error}`);
        callback(error, null);
    });
  } else {
    const result = {
      rows: []
    }
    callback(null, result);
  }
};

