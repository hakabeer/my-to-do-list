const psql_client  = require('../../db/postgres');
const logger = require("../../../logger");
const log = logger.LOG;

module.exports.getAllCategories  = function (callback) {
    const query = `SELECT * FROM public.categories`;
    const input_values = [];
    psql_client.raw(query, input_values)
      .then(function (result) {
        callback(null, result);
      }).catch(function (error) {
        log.error(`Error: ${error}`);
        callback(error, null);
    });
};

module.exports.getAllTaskPriorities  = function (callback) {
    const query = `SELECT * FROM public.task_priorities`;
    const input_values = [];
    psql_client.raw(query, input_values)
        .then(function (result) {
            callback(null, result);
        }).catch(function (error) {
        log.error(`Error: ${error}`);
        callback(error, null);
    });
};

module.exports.getAllTaskStatuses  = function (callback) {
    const query = `SELECT * FROM public.task_statuses`;
    const input_values = [];
    psql_client.raw(query, input_values)
        .then(function (result) {
            callback(null, result);
        }).catch(function (error) {
        log.error(`Error: ${error}`);
        callback(error, null);
    });
};

module.exports.getAllUserStatuses  = function (callback) {
    const query = `SELECT * FROM public.user_statuses`;
    const input_values = [];
    psql_client.raw(query, input_values)
        .then(function (result) {
            callback(null, result);
        }).catch(function (error) {
        log.error(`Error: ${error}`);
        callback(error, null);
    });
};


